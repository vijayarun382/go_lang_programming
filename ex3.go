package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func words2num(input string) string {
	var str = strings.Split(input, " ")
	var op string
	for a := range str {
		if str[a] == "zero" {
			op += "0"
		} else if str[a] == "one" {
			op += "1"
		} else if str[a] == "two" {
			op += "2"
		} else if str[a] == "three" {
			op += "3"
		} else if str[a] == "four" {
			op += "4"
		} else if str[a] == "five" {
			op += "5"
		} else if str[a] == "six" {
			op += "6"
		} else if str[a] == "seven" {
			op += "7"
		} else if str[a] == "eight" {
			op += "8"
		} else if str[a] == "nine" {
			op += "9"
		} else {
			op = "Invalid input"
		}
	}
	return op
}
func main() {
	var input string
	fmt.Println("Enter the input in form of words:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input = scanner.Text()
	input = strings.ToLower(input)
	var output = words2num(input)
	fmt.Println(output)
}