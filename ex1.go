package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func num2Words(inputnum string) string {
	var op string
	var str = strings.Split(inputnum, "")
	for a := range str {
		if str[a] == "0" {
			op += "zero "

		} else if str[a] == "1" {
			op += "one "
		} else if str[a] == "2" {
			op += "two "
		} else if str[a] == "3" {
			op += "three "
		} else if str[a] == "4" {
			op += "four "
		} else if str[a] == "5" {
			op += "five "
		} else if str[a] == "6" {
			op += "six "
		} else if str[a] == "7" {
			op += "seven "
		} else if str[a] == "8" {
			op += "eight "
		} else if str[a] == "9" {
			op += "nine "
		} else if str[a] == " " {
			op += " "
		} else {
			op = "The given input is Not a number"
		}
	}
	return op
}
func main() {
	var inputnum string
	fmt.Println("Enter the Number to be converted into Word:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	inputnum = scanner.Text()
	var output = num2Words(inputnum)
	fmt.Println(output)
}