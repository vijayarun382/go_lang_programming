package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {

	fmt.Println("Enter the real value of complex1")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	real1 := scanner.Text()
	var a1, a2, b1, b2 int64

	if r1, err := strconv.ParseInt(real1, 0, 0); err == nil {
		a1 = r1
	}
	fmt.Println("Enter the imaginary value of complex1")
	scanner1 := bufio.NewScanner(os.Stdin)
	scanner1.Scan()
	imag1 := scanner1.Text()
	if i1, err := strconv.ParseInt(imag1, 0, 0); err == nil {
		b1 = i1
	}
	fmt.Println("Enter the real value of complex2")
	scanner2 := bufio.NewScanner(os.Stdin)
	scanner2.Scan()
	real2 := scanner2.Text()
	if r2, err := strconv.ParseInt(real2, 0, 0); err == nil {
		a2 = r2
	}
	fmt.Println("Enter the imaginary value of complex2")
	scanner3 := bufio.NewScanner(os.Stdin)
	scanner3.Scan()
	imag2 := scanner3.Text()
	if i2, err := strconv.ParseInt(imag2, 0, 0); err == nil {
		b2 = i2
	}
	fmt.Println("Enter the symbol of the operation to be performed:")
	scanner4 := bufio.NewScanner(os.Stdin)
	scanner4.Scan()
	operator := scanner4.Text()

	if operator == "+" {
		oprealval := a1 + a2
		opimagval := b1 + b2
		if opimagval > 0 {
			fmt.Println(strconv.FormatInt(int64(oprealval), 10) + "+" + strconv.FormatInt(int64(opimagval), 10) + "i")
		} else {
			fmt.Println(strconv.FormatInt(int64(oprealval), 10) + strconv.FormatInt(int64(opimagval), 10) + "i")
		}
	}

	if operator == "-" {
		oprealval := a1 - a2
		if b2 < 0 {
			opimagval := b1 + b2
			if opimagval > 0 {
				fmt.Println(strconv.FormatInt(int64(oprealval), 10) + "+" + strconv.FormatInt(int64(opimagval), 10) + "i")
			} else {
				fmt.Println(strconv.FormatInt(int64(oprealval), 10) + strconv.FormatInt(int64(opimagval), 10) + "i")
			}
		} else {
			opimagval := b1 - b2
			if opimagval > 0 {
				fmt.Println(strconv.FormatInt(int64(oprealval), 10) + "+" + strconv.FormatInt(int64(opimagval), 10) + "i")
			} else {
				fmt.Println(strconv.FormatInt(int64(oprealval), 10) + strconv.FormatInt(int64(opimagval), 10) + "i")
			}
		}

	}

	if operator == "*" {
		oprealval := (a1 * a2) - (b1 * b2)
		opimagval := (a1 * b2) + (a2 * b1)
		fmt.Println(strconv.FormatInt(int64(oprealval), 10) + strconv.FormatInt(int64(opimagval), 10) + "i")

	}

}