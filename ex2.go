package main

import (
	"fmt"
)

func maxPower(m int, n int) int {
	var a int
	var copy int = n
	for i := 1; i < m; i++ {
		if m >= n {
			n = n * copy
			if a%2 == 0 {
				a = i + 2
			} else {
				a = i + 1
			}
		}
	}
	return a
}
func main() {
	var m int
	fmt.Println("Enter the value of m:")
	fmt.Scanln(&m)
	var n int
	fmt.Println("Enter the value of n:")
	fmt.Scanln(&n)
	var output = maxPower(m, n)
	fmt.Println(output)
}